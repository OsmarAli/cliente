//*****************************************************************
// File:   Cliente.cpp
// Author: Osmar De La Fuente Maicas
// Date:   enero 2017
// Coms:   cliente con comunicaciÃ³n sÃ­ncrona mediante sockets
//         Compilar el fichero "Makefile" asociado, mediante
//         "make".
//*****************************************************************

#include <iostream>
#include <chrono>
#include <thread>
#include "Socket.h"
#include <stdlib.h>
#include <string>
#include <cstring>
#include <time.h>
#include <fstream>

#define automatico

using namespace std;

const string MENS_FIN("END OF SERVICE");

const int MESSAGE_SIZE = 4001; //mensajes de no mÃ¡s 4000 caracteres

void extraePalabras(Socket socket,int socket_fd){
	std:: ifstream f;
	f.open("lemario.txt");
	srand(time(NULL));
	int numPalabras;
	char sec[256];
	string mensaje;
	do{
		numPalabras=rand()%5+1;
		for(int i=0;i<numPalabras;++i){
			int palabra=(rand()*1000)%86190;
			for(int j=0;j<palabra-1;++j){
				f.ignore(256,'\n');	
			}
			f.getline(sec,256,'\n');
			string s=string(sec);
			if(s=="END OF SERVICE"){
				i=numPalabras;
				mensaje=s;
			}
			else if(i=0){
				mensaje=s;
				f.clear();
			}
			else{
				mensaje=mensaje+" "+s;
				f.clear();
			}		
		}
		// Leer mensaje de la entrada estandar
		cout<<"Escriba una busqueda:"<<endl;
		cout<<mensaje<<endl;
		// Enviamos el mensaje
	    int send_bytes = socket.Send(socket_fd, mensaje);

	    if(send_bytes == -1){
			cerr << "Error al enviar datos: " << strerror(errno) << endl;
			// Cerramos el socket
			socket.Close(socket_fd);
			exit(1);
		}

		if(mensaje != MENS_FIN){
		    // Buffer para almacenar la respuesta
		    string buffer;

		    // Recibimos la respuesta del servidor  
		    int read_bytes = socket.Recv(socket_fd, buffer, MESSAGE_SIZE);

		    // Mostramos la respuesta
		    cout << "Mensaje enviado: \n'" << mensaje << "'" << endl;
		    cout << "\nRespuesta: \n" << buffer << endl;
		}
	} while(mensaje != MENS_FIN);

    // Cerramos el socket
    int error_code = socket.Close(socket_fd);
    if(error_code == -1){
		cerr << "Error cerrando el socket: " << strerror(errno) << endl;
    }
}

int main(int argc,char* argv[]) {
    // DirecciÃ³n y nÃºmero donde escucha el proceso servidor
    string SERVER_ADDRESS = "localhost";
	
    int SERVER_PORT = atoi(argv[1]);

	SERVER_ADDRESS=argv[2];
	// CreaciÃ³n del socket con el que se llevarÃ¡ a cabo
	// la comunicaciÃ³n con el servidor.
	Socket socket(SERVER_ADDRESS, SERVER_PORT);

    // Conectamos con el servidor. Probamos varias conexiones
	const int MAX_ATTEMPS = 10;
	int count = 0;
	int socket_fd;
	do {
		// ConexiÃ³n con el servidor
    	socket_fd = socket.Connect();
    	count++;

    	// Si error --> esperamos 1 segundo para reconectar
    	if(socket_fd == -1){
    	    this_thread::sleep_for(chrono::seconds(1));
    	}
    } while(socket_fd == -1 && count < MAX_ATTEMPS);

    // Chequeamos si se ha realizado la conexiÃ³n
    if(socket_fd == -1){
    	return socket_fd;
    }
	#ifdef automatico
		extraePalabras(socket,socket_fd);
	#else
		do{
			string mensaje;
			// Leer mensaje de la entrada estandar
			cout<<"Escriba una busqueda:"<<endl;
			getline(cin, mensaje);
			// Enviamos el mensaje
	    	int send_bytes = socket.Send(socket_fd, mensaje);

		    if(send_bytes == -1){
				cerr << "Error al enviar datos: " << strerror(errno) << endl;
				// Cerramos el socket
				socket.Close(socket_fd);
				exit(1);
			}

			if(mensaje != MENS_FIN){
			    // Buffer para almacenar la respuesta
			    string buffer;
	
			    // Recibimos la respuesta del servidor  
			    int read_bytes = socket.Recv(socket_fd, buffer, MESSAGE_SIZE);

			    // Mostramos la respuesta
			    cout << "Mensaje enviado: \n'" << mensaje << "'" << endl;
			    cout << "\nRespuesta: \n" << buffer << endl;
			}
		} while(mensaje != MENS_FIN);

    	// Cerramos el socket
   		int error_code = socket.Close(socket_fd);
   		if(error_code == -1){
			cerr << "Error cerrando el socket: " << strerror(errno) << endl;
  		}

    	return error_code;
	#endif
}